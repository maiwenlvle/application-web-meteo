//Date element
let date = new Date()
//Map of the week numbers and the name of the day
let weekNumber = {0: "Dimanche", 1: "Lundi", 2: "Mardi", 3: "Mercredi", 4: "Jeudi", 5: "Vendredi", 6: "Samedi"};
//Map of the month numbers and the name of the day
let monthNumber = {0: "Jan", 1: "Fev", 2: "Mar", 3: "Avr", 4: "Mai", 5: "Juin", 6: "Juil", 7: "Aou", 8: "Sep", 9: "oct", 10: "Nov", 11: "Dec"};

//key for openWeatherMap
let key = "ee07e2bf337034f905cde0bdedae3db8";

var app = angular.module("mainCtrl", []);

/**
 * The function isLocalStorageEmpty() is used to check if the local storage is empty or not.
 * @returns {boolean} true if the local storage is empty, false otherwise.
 */
function isLocalStorageEmpty() {
    let isEmpty = JSON.parse(localStorage.getItem("cities"));
    return isEmpty == null;
}

/**
 * meteovillesCtrl is the controller of the principal page. It's used to show the simple weather of the cities that
 * have been save in the local storage.
 */
app.controller('meteovillesCtrl', function($scope, $http) {

    let cities = []
    if (!isLocalStorageEmpty()) {
        cities = JSON.parse(localStorage.getItem("cities"));
    } else {
        //show a text if there are no cities.
        $scope.title = "Aucune ville ajoutées !";
    }
    $scope.cities = cities;

    //parcours les villes
    $scope.cities.forEach(city => {

        $http({
            method : "GET",
            url : "https://api.openweathermap.org/data/2.5/forecast/daily?q=" + city.name + "&cnt=7&appid=" + key + "&units=metric&lang=fr"
        }).then(function mySuccess(response) {
            city.cityName = response.data.city.name;
            city.day = weekNumber[date.getDay()];
            city.date = date.getDate() + " " + monthNumber[date.getMonth()] + " " + date.getFullYear();
            city.temperature = response.data.list[0].temp.day.toFixed(0)+"°C";
            city.pressure = response.data.list[0].pressure+" hPa";
            city.humidity = response.data["list"][0]["humidity"]+" %";
            city.wind = response.data.list[0].speed+" km/h";
            city.description = response.data.list[0].weather[0].description;
            city.rain = response.data.list[0].rain+" %";
            city.icon = "http://openweathermap.org/img/w/" + response.data.list[0].weather[0].icon + ".png"

        }, function myError(response) {
            console.log(response.error);
        });
    })
});

/**
 * Controller of the page of previsions.
 */
app.controller('previsionsCtrl', function($scope, $http, $route, $window) {

    if($route.current.params.id == undefined) {
        $window.location.href = "#";
    } else {

        $scope.days = [
            {"id" : 0},
            {"id" : 1},
            {"id" : 2},
            {"id" : 3},
            {"id" : 4},
            {"id" : 5},
            {"id" : 6}
        ]

        //parcours les jours
        $scope.days.forEach(day => {

            let cities = JSON.parse(localStorage.getItem("cities"));
            let city = cities.filter(function(el) {
                return el.id == $route.current.params.id;
            });

            $http({
                method : "GET",
                url : "https://api.openweathermap.org/data/2.5/forecast/daily?q=" + city[0].name + "&cnt=7&appid=ee07e2bf337034f905cde0bdedae3db8&units=metric&lang=fr"
            }).then(function mySuccess(response) {

                day.name = response.data.city.name;
                day.day = weekNumber[(date.getDay()+day.id)%7];
                day.date = (date.getDate()+day.id)%31 + " " + monthNumber[date.getMonth()] + " " + date.getFullYear();
                day.temperature = response.data.list[day.id].temp.day.toFixed(0)+"°C";
                day.pressure = response.data.list[day.id].pressure+" hPa";
                day.humidity = response.data["list"][day.id]["humidity"]+" %";
                day.wind = response.data.list[day.id].speed+" km/h";
                day.description = response.data.list[day.id].weather[0].description;
                let rain = response.data.list[day.id].rain;
                if (rain == undefined) {
                    day.rain = "0%";
                } else {
                    day.rain = response.data.list[day.id].rain+" %";
                }
                day.icon = "http://openweathermap.org/img/w/" + response.data.list[day.id].weather[0].icon + ".png"

            }, function myError(response) {
                console.log(response.error);
            });
        })
    }
});

/**
 * Controller of the page of cities.
 */
app.controller('villesCtrl', function($scope, $http) {

    let cities = JSON.parse(localStorage.getItem("cities"));
    $scope.addCity = addCity;
    $scope.removeCity = removeCity;
    $scope.cities = cities;

    /**
     * The function addCity(city) is used to add a city to the localStorage.
     * @param city
     */
    function addCity(city) {

        $http({
            method: 'GET',
            url: 'http://api.openweathermap.org/data/2.5/weather?q=' + city + "&APPID=2370a9749f38195f07d3bbefd145b74c&units=metric&lang=fr",
        }).then(function successCallback(response) {
            let city = {
                "name": response.data.name,
                "id": response.data.id,
            };
            let cities = JSON.parse(localStorage.getItem("cities"));
            if(isLocalStorageEmpty()) {
                cities = [];
            }
            if(!cities.some(e => e.name === city["name"])) {
                cities.push(city);
                localStorage.setItem("cities", JSON.stringify(cities));
                M.toast({html: city.name + " a été ajoutée !", classes: 'green rounded'});
                location.reload()
            } else {
                M.toast({html: city.name + " est déjà ajoutée ! ", classes: 'red rounded'});
            }
        });
    }

    /**
     * The function removeCity is used to remove a city from the local storage.
     * @param id of the city to remove.
     */
    function removeCity(id) {
        cities.splice(id,1);
        localStorage.setItem("cities", JSON.stringify(cities));
        M.toast({html: city.name + " a été suprimée !", classes: 'green rounded'});
    }
});